# Flipper File System Viewer plugin

Views the file system on the device, and allows individual files to be downloaded.

This is currently incomplete, and should only be used for educational purposes.

## flipper-plugin-fs-viewer

The plugin for Flipper Desktop.

For development:

- Stop Flipper if it currently running.
- Add this repository's `packages` directory [to the flipper configuration](https://fbflipper.com/docs/tutorial/js-setup#dynamic-plugin-loading).
- Check out the [Flipper repository](https://github.com/facebook/flipper)
- Follow the instructions to [Run the Desktop app from source](https://github.com/facebook/flipper#running-from-source).
- Flipper will automatically watch the `packages` directory and recompile flipper-plugin-fs-viewer when it changes.

## react-native-flipper-fs-viewer

The Android/iOS Flipper plugin that provides directory listings or files to `flipper-plugin-fs-viewer`.  This is only made as a React Native package so that it can be conveniently autolinked, but the plugin classes could be used in a native app too.

Android
```Java
import com.reactlibrary.FileSystemViewerFlipperPlugin;
...
client.addPlugin(new FileSystemViewerFlipperPlugin(context));
```

iOS:
```ObjC
#import <FileSystemViewerFlipperPlugin.h>
...
[client addPlugin:[[FileSystemViewerFlipperPlugin alloc] init]];
```

For development you can safely use yarn link, since there is no JavaScript that needs rebundling:

```bash
cd $THIS_REPO/packages/react-native-flipper-fs-viewer
yarn link
cd $APP_REPO
yarn link react-native-flipper-fs-viewer
```
