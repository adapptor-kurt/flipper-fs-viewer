import {
  ManagedTable,
  TableColumns,
  TableColumnSizes,
  TableHighlightedRows,
  TableRows
} from "flipper";
import React, { useCallback, useMemo } from "react";
import { Directory, Item, NO_SELECTION, PathSelection } from "../files";
import FileListingName from "./FileListingName";

interface Props {
  roots: Directory[];
  onPathSelected: (path: string[] | undefined) => void;
  open: PathSelection;
  onOpenToggle: (path: string[]) => void;
}

const COLUMN_SIZES: TableColumnSizes = {
  name: "flex",
  size: "15%",
};
const COLUMNS: TableColumns = {
  name: {
    value: "Name",
    sortable: false,
    resizable: true,
  },
  size: {
    value: "Size",
    sortable: false,
    resizable: true,
  },
};

function splitPathKey(key: string): string[] {
  return key.split("/").map((name) => decodeURIComponent(name));
}

const FileListingTable: React.FC<Props> = ({
  roots,
  onPathSelected,
  open,
  onOpenToggle,
}) => {
  const onOpenToggleCallback = useCallback(
    (itemKey: string) => {
      onOpenToggle(splitPathKey(itemKey));
    },
    [onOpenToggle]
  );

  const rows = useMemo(() => {
    const rows: TableRows = [];

    function addItem(
      base: string,
      indent: number,
      item: Item,
      parentSelection: PathSelection
    ) {
      const key = base + encodeURIComponent(item.name);
      const itemSelection = Object.prototype.hasOwnProperty.call(parentSelection.children, item.name)
        ? parentSelection.children[item.name]
        : NO_SELECTION

      rows.push({
        key,
        columns: {
          name: {
            value: (
              <FileListingName
                itemKey={key}
                item={item}
                indent={indent}
                open={itemSelection.open}
                onOpenToggle={onOpenToggleCallback}
              />
            ),
          },
          size: {
            value: item.size,
            align: "flex-end",
          },
        },
      });

      if (itemSelection.open && item.listing) {
        const base = key + "/";
        for (const child of item.listing) {
          addItem(
            base,
            indent + 1,
            child,
            itemSelection
          );
        }
      }
    }

    for (const root of roots) {
      addItem("", 0, root, open);
    }

    return rows;
  }, [roots, open, onOpenToggleCallback]);

  const onRowHighlighted = useCallback(
    (keys: TableHighlightedRows) => {
      if (onPathSelected) {
        const path = keys.length > 0 ? splitPathKey(keys[0]) : undefined;
        onPathSelected(path);
      }
    },
    [onPathSelected]
  );

  return (
    <ManagedTable
      floating={false}
      multiline={true}
      columnSizes={COLUMN_SIZES}
      columns={COLUMNS}
      onRowHighlighted={onRowHighlighted}
      multiHighlight={false}
      rows={rows}
      stickyBottom={true}
    />
  );
};

export default FileListingTable;
