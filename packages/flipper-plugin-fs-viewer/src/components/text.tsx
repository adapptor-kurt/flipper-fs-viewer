import { styled, Text } from "flipper";

export const NameText = styled(Text)((props) => ({
  whiteSpace: "nowrap",
  textOverflow: "ellipsis",
}));

export const AliasText = styled(Text)((props) => ({
  fontSize: "80%",
  background: "rgba(0,0,0,0.3)",
  color: "white",
  borderRadius: "0.2em",
  padding: "0.2em 0.4em",
}));
