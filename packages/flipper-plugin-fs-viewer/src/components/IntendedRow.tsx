import { FlexRow, styled } from "flipper";

const IntendedRow = styled(FlexRow)<{ indent: number }>((props) => ({
  flexDirection: "row",
  marginLeft: `${props.indent * 1.5}em`,
}));

export default IntendedRow;
