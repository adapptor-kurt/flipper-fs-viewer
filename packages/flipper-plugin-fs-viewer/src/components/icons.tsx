import { Glyph, styled, View } from "flipper";
import React from "react";

const FileIconBox = styled(View)((props) => ({
  width: "1em",
  marginRight: "0.2em",
}));

export const FileIcon: React.FC<{
  visible: boolean;
  open: boolean;
  onOpenToggle?: () => void;
}> = ({ visible, open, onOpenToggle }) => {
  return (
    <FileIconBox onClick={onOpenToggle}>
      {visible && (
        <Glyph
          size={8}
          name={open ? "chevron-down" : "chevron-right"}
          color={"#010101"}
        />
      )}
    </FileIconBox>
  );
};
