import React, { useCallback } from "react";
import { Item } from "../files";
import { FileIcon } from "./icons";
import IntendedRow from "./IntendedRow";
import { AliasText, NameText } from "./text";

const FileListingName: React.FC<{
  itemKey: string;
  item: Item;
  indent: number;
  open: boolean;
  onOpenToggle?: (key: string) => void;
}> = ({ itemKey, item, indent, open, onOpenToggle }) => {
  const onOpenToggleCallback = useCallback(() => {
    if (onOpenToggle) {
      onOpenToggle(itemKey);
    }
  }, [itemKey, onOpenToggle]);

  return (
    <IntendedRow indent={indent}>
      <FileIcon visible={!!item.listing} open={open} onOpenToggle={onOpenToggleCallback} />
      <NameText>
        {!!item.alias && (
          <>
            <AliasText>{item.alias}</AliasText>{" "}
          </>
        )}
        {item.name}
      </NameText>
    </IntendedRow>
  );
};

export default FileListingName;
