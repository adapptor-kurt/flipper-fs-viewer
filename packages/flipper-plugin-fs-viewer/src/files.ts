export type Item = File | Directory;

export interface File {
  type: "file";
  name: string;
  size: number;
  alias?: undefined;
  listing?: undefined;
}

export interface Directory {
  type: "dir";
  name: string;
  size?: undefined;
  alias?: string;
  listing: Array<Item>;
}

export interface PathSelection {
  open: boolean;
  children: { [name: string]: PathSelection };
}

export const NO_SELECTION: PathSelection = {
  open: false,
  children: {},
};

export function sortDirectory(dir: Directory): Directory {
  return {
    ...dir,
    listing: [...dir.listing]
      .sort((a, b) => a.name.localeCompare(b.name))
      .map((item) => {
        if (item.type != "dir") {
          return item;
        } else {
          return sortDirectory(item);
        }
      }),
  };
}

export function getItem(
  items: Item[] | undefined,
  path: string[],
  index: number = 0
): Item | undefined {
  const name = path[index];
  const found = items?.find((item) => item.name === name);
  if (found && index + 1 < path.length) {
    return getItem(found.listing, path, index + 1);
  } else {
    return found;
  }
}

export function toggleOpen(
  selection: PathSelection,
  path: string[],
  index: number = 0
): PathSelection {
  if (index >= path.length) {
    return {
      ...selection,
      open: !selection.open,
    };
  }

  return {
    ...selection,
    children: {
      ...selection.children,
      [path[index]]: toggleOpen(
        Object.prototype.hasOwnProperty.call(selection.children, path[index])
          ? selection.children[path[index]]
          : NO_SELECTION,
        path,
        index + 1
      ),
    },
  };
}
