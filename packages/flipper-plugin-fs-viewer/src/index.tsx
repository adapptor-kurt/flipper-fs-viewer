import { remote, SaveDialogOptions } from "electron";
import {
  Button,
  FlexColumn,
  FlipperPlugin,
  KeyboardActions,
  Toolbar,
} from "flipper";
import React from "react";
import FileListingTable from "./components/FileListingTable";
import {
  Directory,
  getItem,
  NO_SELECTION,
  PathSelection,
  sortDirectory,
  toggleOpen,
} from "./files";
import fs from "fs";

interface State {
  selectedPath?: string[];
}

interface RefreshPayload {
  roots: Directory[];
}

type PersistedState = {
  roots: Directory[];
  selection: PathSelection;
};

export default class FileSystemViewerFlipperPlugin extends FlipperPlugin<
  State,
  any,
  PersistedState
> {
  static keyboardActions: KeyboardActions = [];

  static defaultPersistedState: PersistedState = {
    roots: [],
    selection: NO_SELECTION
  };

  static persistedStateReducer = (
    persistedState: PersistedState,
    method: string,
    data: any
  ): PersistedState => {
    switch (method) {
      case "refresh": {
        const payload: RefreshPayload = data;
        return {
          ...persistedState,
          roots: payload.roots.map(sortDirectory),
        };
      }
      default:
        console.log("Unknown method:", method, data);
        return persistedState;
    }
  };

  public state: State = {
  };

  public onKeyboardAction = (action: string) => {};

  private onRefreshPress = async () => {
    await this.client.call("refresh");
  };

  private onDownloadPress = async () => {
    const { selectedPath } = this.state;
    if (!selectedPath) {
      return;
    }
    const item = getItem(this.props.persistedState.roots, selectedPath);
    if (!item) {
      return;
    }

    const options: SaveDialogOptions = {
      defaultPath: item.name,
    };
    const value = await remote.dialog.showSaveDialog(options);
    if (!value.canceled && value.filePath) {
      const file = await fs.createWriteStream(value.filePath, { encoding: "binary" });

      let more = true;
      let from = 0;
      while (more) {
        const result = await this.client.call("download", { path: selectedPath.join('/'), from });
        more = result.more;
        file.write(result.data);
        from += result.data.length;
      } while (more);

      file.close();
    }
  };

  private onPathSelected = (selectedPath: string[] | undefined) => {
    this.setState({
      selectedPath,
    });
  };

  private onOpenToggle = (path: string[]) => {
    this.props.setPersistedState({
      selection: toggleOpen(this.props.persistedState.selection, path),
    });
  };

  public render() {
    const selectedItem = this.state.selectedPath && getItem(this.props.persistedState.roots, this.state.selectedPath);

    return (
      <FlexColumn grow={true}>
        <Toolbar position="top" key="toolbar">
          <Button onClick={this.onRefreshPress}>Refresh</Button>
          <Button
            onClick={this.onDownloadPress}
            disabled={!selectedItem || selectedItem.type !== 'file'}
          >
            Download
          </Button>
        </Toolbar>
        <FileListingTable
          roots={this.props.persistedState.roots}
          onPathSelected={this.onPathSelected}
          open={this.props.persistedState.selection}
          onOpenToggle={this.onOpenToggle}
        />
      </FlexColumn>
    );
  }
}
