#if DEBUG

#import "FileSystemViewerFlipperPlugin.h"

#import <FlipperKit/FlipperClient.h>
#import <FlipperKit/FlipperConnection.h>
#import <FlipperKit/FlipperResponder.h>
#import <FlipperKit/SKMacros.h>

static NSArray<NSURLResourceKey> *_resourceKeys;
static size_t DOWNLOAD_CHUNK_SIZE = 1024 * 1024;

@implementation FileSystemViewerFlipperPlugin {
  id<FlipperConnection> _connection;
}

+ (void)initialize {
  if (_resourceKeys == nil) {
    _resourceKeys = @[NSURLNameKey, NSURLIsDirectoryKey, NSURLFileSizeKey, NSURLIsHiddenKey, NSURLContentModificationDateKey];
  }
}

- (NSString*)identifier {
  return @"fs-viewer";
}

- (void)didConnect:(id<FlipperConnection>)connection {
  _connection = connection;
  
  __weak typeof(self) weakSelf = self;
  [connection receive:@"refresh" withBlock:^(NSDictionary* params, id<FlipperResponder> responder) {
    [weakSelf refresh];
    [responder success:nil];
  }];
  [connection receive:@"download" withBlock:^(NSDictionary* params, id<FlipperResponder> responder) {
    long from = [(NSNumber *)params[@"from"] longValue];
    NSString *path = params[@"path"];
    FILE *file = NULL;
    void *data = NULL;
    BOOL success = false;
    
    file = fopen(path.UTF8String, "rb");
    if (file) {
      fseek(file, 0, SEEK_END);
      long fileLength = ftell(file);
      fseek(file, from, SEEK_SET);
      size_t expectedCount = MIN((size_t)(fileLength - from), DOWNLOAD_CHUNK_SIZE);
      void *data = malloc(expectedCount);
      if (data) {
        size_t readCount = fread(data, 1, expectedCount, file);
        success = true;
        NSString *dataStr = readCount > 0 ? [[NSString alloc] initWithBytes:data length:readCount encoding:NSISOLatin1StringEncoding] : @"";
        [responder success:@{
          @"data": dataStr,
          @"more": @(readCount > 0 && from + readCount < fileLength)
        }];
      }
    }
    
    free(data);
    fclose(file);
    if (!success) {
      [responder error:nil];
    }
  }];
  
  [self refresh];
}

- (void)didDisconnect {
  _connection = nil;
}

- (BOOL)runInBackground {
  return true;
}

- (void)refresh {
  NSMutableArray *roots = [[NSMutableArray alloc] init];
  [self addDomain:NSUserDomainMask alias:@"user" to:roots];
  [self addDomain:NSLocalDomainMask alias:@"local" to:roots];
  [self addDomain:NSNetworkDomainMask alias:@"network" to:roots];
  [self addDomain:NSSystemDomainMask alias:@"system" to:roots];
  
  if (_connection) {
    [_connection send:@"refresh" withParams:@{
      @"roots": roots
    }];
  }
}

- (NSArray *)getDirectoryListingForURL:(NSURL *)url {
  NSError *error;
  NSArray<NSURL *> *files = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:url
                                includingPropertiesForKeys:_resourceKeys
                                                   options:NSDirectoryEnumerationSkipsSubdirectoryDescendants
                                                     error:&error];
  if (!files) {
    return nil;
  }
  
  NSMutableArray *listing = [[NSMutableArray alloc] init];
  for (NSURL *file in files) {
    NSDictionary<NSURLResourceKey, id> *resources = [file resourceValuesForKeys:_resourceKeys error:&error];
    if (resources) {
      NSString *name = resources[NSURLNameKey];
      if (!name) {
        name = [file lastPathComponent];
      }
      NSDate *modified = resources[NSURLContentModificationDateKey];
      NSNumber *modifiedTime = modified ? @(floor([modified timeIntervalSince1970] * 1000)) : nil;
      
      if ([resources[NSURLIsDirectoryKey] boolValue]) {
        NSArray *fileListing = [self getDirectoryListingForURL:file];
        [listing addObject:@{
          @"type": @"dir",
          @"name": name,
          @"hidden": resources[NSURLIsHiddenKey],
          @"listing": fileListing ? fileListing : @[], // TODO: We should show an error here
          @"modifiedTime": modifiedTime,
        }];
      } else {
        [listing addObject:@{
          @"type": @"file",
          @"name": name,
          @"hidden": resources[NSURLIsHiddenKey],
          @"size": resources[NSURLFileSizeKey],
          @"modifiedTime": modifiedTime,
        }];
      }
    }
  }
  return listing;
}

- (void)addDomain:(NSSearchPathDomainMask)domain alias:(NSString *)alias to:(NSMutableArray *)roots {
  [self addRoot:domain :alias dir:NSApplicationDirectory :@"app" to:roots];
  [self addRoot:domain :alias dir:NSDemoApplicationDirectory :@"demo-app" to:roots];
  [self addRoot:domain :alias dir:NSDeveloperApplicationDirectory :@"dev-app" to:roots];
  [self addRoot:domain :alias dir:NSAdminApplicationDirectory :@"admin-app" to:roots];
  [self addRoot:domain :alias dir:NSLibraryDirectory :@"library" to:roots];
  [self addRoot:domain :alias dir:NSDeveloperDirectory :@"dev" to:roots];
  [self addRoot:domain :alias dir:NSUserDirectory :@"user" to:roots];
  [self addRoot:domain :alias dir:NSDocumentationDirectory :@"docs" to:roots];
  [self addRoot:domain :alias dir:NSCoreServiceDirectory :@"core" to:roots];
  [self addRoot:domain :alias dir:NSAutosavedInformationDirectory :@"autosaved" to:roots];
  [self addRoot:domain :alias dir:NSDesktopDirectory :@"desktop" to:roots];
  [self addRoot:domain :alias dir:NSCachesDirectory :@"cache" to:roots];
  [self addRoot:domain :alias dir:NSApplicationSupportDirectory :@"app-support" to:roots];
  [self addRoot:domain :alias dir:NSDownloadsDirectory :@"downloads" to:roots];
  [self addRoot:domain :alias dir:NSInputMethodsDirectory :@"input-methods" to:roots];
  [self addRoot:domain :alias dir:NSMoviesDirectory :@"movies" to:roots];
  [self addRoot:domain :alias dir:NSMusicDirectory :@"music" to:roots];
  [self addRoot:domain :alias dir:NSPicturesDirectory :@"pictures" to:roots];
  [self addRoot:domain :alias dir:NSPrinterDescriptionDirectory :@"printer" to:roots];
  [self addRoot:domain :alias dir:NSSharedPublicDirectory :@"public" to:roots];
  [self addRoot:domain :alias dir:NSPreferencePanesDirectory :@"prefs" to:roots];
  [self addRoot:domain :alias dir:NSItemReplacementDirectory :@"replacement" to:roots];
  if (@available(iOS 11.0, *)) {
    [self addRoot:domain :alias dir:NSTrashDirectory :@"trash" to:roots];
  }
}

- (void)addRoot:(NSSearchPathDomainMask)domain :(NSString *)domainAlias dir:(NSSearchPathDirectory)dir :(NSString *)dirAlias to:(NSMutableArray *)roots {
  NSArray<NSURL *> *urls = [[NSFileManager defaultManager] URLsForDirectory:dir inDomains:domain];
  
  for (NSURL *url in urls) {
    NSArray *listing = [self getDirectoryListingForURL:url];
    if (listing) {
      [roots addObject:@{
        @"type": @"dir",
        @"alias": [NSString stringWithFormat:@"%@ %@", domainAlias, dirAlias],
        @"name": url.fileURL ? url.path : url.absoluteString,
        @"listing": listing,
      }];
    }
  }
}

@end

#endif
