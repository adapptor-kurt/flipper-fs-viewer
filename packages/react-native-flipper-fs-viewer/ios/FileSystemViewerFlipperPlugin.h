#if DEBUG

#import <Foundation/Foundation.h>
#import <FlipperKit/FlipperPlugin.h>

@interface FileSystemViewerFlipperPlugin : NSObject <FlipperPlugin>
  
@end

#endif
