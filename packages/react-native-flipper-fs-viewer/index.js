import { NativeModules } from 'react-native';

const { FlipperFsViewer } = NativeModules;

export default FlipperFsViewer;
