package com.reactlibrary;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.flipper.core.FlipperArray;
import com.facebook.flipper.core.FlipperConnection;
import com.facebook.flipper.core.FlipperObject;
import com.facebook.flipper.core.FlipperPlugin;
import com.facebook.flipper.core.FlipperReceiver;
import com.facebook.flipper.core.FlipperResponder;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

public class FileSystemViewerFlipperPlugin implements FlipperPlugin {
    private static final String TAG = "FSViewerFlipperPlugin";
    private static final int DOWNLOAD_CHUNK_SIZE = 1024 * 1024;

    private @NonNull Context mContext;
    private @Nullable FlipperConnection mConnection;

    public FileSystemViewerFlipperPlugin(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public String getId() {
        return "fs-viewer";
    }

    @Override
    public void onConnect(FlipperConnection connection) throws Exception {
        mConnection = connection;
        mConnection.receive("refresh", new FlipperReceiver() {
            @Override
            public void onReceive(FlipperObject params, FlipperResponder responder) throws Exception {
                refresh();
                responder.success();
            }
        });
        mConnection.receive("download", new FlipperReceiver() {
            @Override
            public void onReceive(FlipperObject params, FlipperResponder responder) throws Exception {
                long from = params.getLong("from");
                File file = new File(params.getString("path"));

                // Work out how many bytes are expected, then read that many
                int expectedCount = Math.min((int) (file.length() - from), DOWNLOAD_CHUNK_SIZE);
                byte[] bytes = new byte[expectedCount];
                RandomAccessFile raf = new RandomAccessFile(file, "r");
                raf.seek(from);
                int readCount = raf.read(bytes);
                Log.d(TAG, String.format("Read %s bytes from %s: %s", readCount, from, file.getName()));

                responder.success(new FlipperObject.Builder()
                    .put("data", readCount > 0 ? new String(bytes, 0, readCount, StandardCharsets.ISO_8859_1) : "")
                    .put("more", readCount > 0 && from + readCount < file.length())
                    .build());
            }
        });

        refresh();
    }

    @Override
    public void onDisconnect() throws Exception {
        mConnection = null;
    }

    @Override
    public boolean runInBackground() {
        return true;
    }

    private void refresh() {
        FlipperArray.Builder roots = new FlipperArray.Builder();
        this.addRoot(roots, "cache", mContext.getCacheDir());
        this.addRoot(roots, "files", mContext.getFilesDir());
        FlipperObject data = new FlipperObject.Builder()
            .put("roots", roots.build())
            .build();

        if (mConnection != null) {
            mConnection.send("refresh", data);
        }
    }

    private FlipperArray getDirectoryListing(@NonNull File dir) {
        FlipperArray.Builder listing = new FlipperArray.Builder();

        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    listing.put(this.getDirectory(file));
                } else if (file.isFile()) {
                    listing.put(this.getFile(file));
                }
            }
        }

        return listing.build();
    }

    private FlipperObject.Builder putCommonProps(FlipperObject.Builder builder, @NonNull File file) {
        return builder
            .put("modifiedTime", file.lastModified())
            .put("hidden", file.isHidden());
    }

    private FlipperObject.Builder putCommonDirectoryProps(FlipperObject.Builder builder, @NonNull File dir) {
        return putCommonProps(builder, dir)
            .put("listing", this.getDirectoryListing(dir));
    }

    private void addRoot(@NonNull FlipperArray.Builder roots, @NonNull String alias, @Nullable File dir) {
        if (dir == null) {
            return;
        }

        roots.put(putCommonDirectoryProps(new FlipperObject.Builder(), dir)
            .put("type", "dir")
            .put("name", dir.getAbsolutePath())
            .put("alias", alias)
            .build());
    }

    private FlipperObject getDirectory(@NonNull File dir) {
        return putCommonDirectoryProps(new FlipperObject.Builder(), dir)
            .put("type", "dir")
            .put("name", dir.getName())
            .build();
    }

    private FlipperObject getFile(@NonNull File file) {
        return putCommonProps(new FlipperObject.Builder(), file)
            .put("type", "file")
            .put("name", file.getName())
            .put("size", file.length())
            .build();
    }
}
